package com.example.cbunk.myapplication.weather;

import static org.mockito.Matchers.eq;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.cbunk.myapplication.weather.model.pojo.ForecastDailyResponse;
import com.example.cbunk.myapplication.weather.model.pojo.WeatherResponse;

import rx.Observable;
import rx.Scheduler;

import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;

import rx.schedulers.Schedulers;

public class WeatherPresenterTest {

    private static final int BERLIN_CITY_ID = 2950159;

    @Mock
    private WeatherContract.Model weatherModel;

    @Mock
    private WeatherContract.View weatherView;

    @Mock
    private WeatherResponse weatherResponse;

    @Mock
    private ForecastDailyResponse forecastDailyResponse;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to perform further
     * actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<WeatherContract.Model.LoadCurrentWeatherCallback> loadCurrentWeatherCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<WeatherContract.Model.LoadWeatherForecastCallback> loadWeatherForecastCallbackArgumentCaptor;

    private WeatherPresenter weatherPresenter;

    @Before
    public void setupWeatherPresenter() {

        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
                @Override
                public Scheduler getMainThreadScheduler() {
                    return Schedulers.immediate();
                }
            });

        weatherPresenter = spy(new WeatherPresenter(weatherView, weatherModel));
    }

    @Test
    public void testGetCurrentWeather() {
        when(weatherModel.getCurrentWeather(BERLIN_CITY_ID)).thenReturn(Observable.just(weatherResponse));

        weatherPresenter.getCurrentWeather(BERLIN_CITY_ID);

        // Callback is captured and invoked with stubbed weatherResponse
        verify(weatherModel).getCurrentWeather(eq(BERLIN_CITY_ID));

        verify(weatherView).showCurrentWeather(weatherResponse);
    }

    @Test
    public void testGetWeatherForecast() {
        when(weatherModel.getWeatherForecast(BERLIN_CITY_ID)).thenReturn(Observable.just(forecastDailyResponse));

        weatherPresenter.getWeatherForecast(BERLIN_CITY_ID);

// verify(weatherModel).getWeatherForecast(eq(BERLIN_CITY_ID),
// loadWeatherForecastCallbackArgumentCaptor.capture());
// loadWeatherForecastCallbackArgumentCaptor.getValue().onWeatherForecastLoaded(forecastDailyResponse);

        verify(weatherModel).getWeatherForecast(eq(BERLIN_CITY_ID));
        verify(weatherView).showWeatherForecast(forecastDailyResponse);
    }

    @After
    public void tearDown() {
        RxAndroidPlugins.getInstance().reset();
    }
}
