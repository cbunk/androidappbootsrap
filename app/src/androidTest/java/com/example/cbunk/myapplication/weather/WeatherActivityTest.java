package com.example.cbunk.myapplication.weather;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import com.example.cbunk.myapplication.R;

import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import android.test.suitebuilder.annotation.LargeTest;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class WeatherActivityTest {

    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     *
     * <p>Rules are interceptors which are executed for each test method and are important building blocks of Junit
     * tests.
     */
    @Rule
    public ActivityTestRule<WeatherActivity> weatherActivityTestRule = new ActivityTestRule<>(WeatherActivity.class);

    @Before
    public void registerIdlingResource() {
        Espresso.registerIdlingResources(weatherActivityTestRule.getActivity().getCountingIdlingResource());
    }

    @After
    public void unregisterIdlingResource() {
        Espresso.unregisterIdlingResources(weatherActivityTestRule.getActivity().getCountingIdlingResource());
    }

    @Test
    public void currentWeather() throws Exception {

        // Check if the add note screen is displayed
        onView(withId(R.id.weather_activity_city_textview)).check(matches(isDisplayed()));
        onView(withId(R.id.weather_activity_description_textview)).check(matches(isDisplayed()));
        onView(withId(R.id.weather_activity_temperature_textview)).check(matches(isDisplayed()));
        onView(withId(R.id.weather_activity_recycler_view)).check(matches(isDisplayed()));
    }
}
