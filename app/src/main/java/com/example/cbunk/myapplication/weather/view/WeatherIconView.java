package com.example.cbunk.myapplication.weather.view;

import android.content.Context;

import android.util.AttributeSet;

import com.example.cbunk.myapplication.core.view.PicassoImageView;

/**
 * Created by cbunk on 14.01.16.
 */
public class WeatherIconView extends PicassoImageView {
    public WeatherIconView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    public WeatherIconView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public WeatherIconView(final Context context) {
        super(context);
    }

    public void showIcon(final String iconId) {
        setImageUrl("http://openweathermap.org/img/w/" + iconId + ".png");
    }
}
