
package com.example.cbunk.myapplication.weather.model.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class WeatherResponse {

    @SerializedName("coord")
    @Expose
    private Coord coord;
    @SerializedName("weather")
    @Expose
    private List<Weather> weather = new ArrayList<Weather>();
    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("main")
    @Expose
    private Main main;
    @SerializedName("wind")
    @Expose
    private Wind wind;
    @SerializedName("clouds")
    @Expose
    private Clouds clouds;
    @SerializedName("rain")
    @Expose
    private Rain rain;
    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("sys")
    @Expose
    private Sys sys;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cod")
    @Expose
    private Integer cod;

    private WeatherResponse(final Builder builder) {
        setCoord(builder.coord);
        setWeather(builder.weather);
        setBase(builder.base);
        setMain(builder.main);
        setWind(builder.wind);
        setClouds(builder.clouds);
        setRain(builder.rain);
        setDt(builder.dt);
        setSys(builder.sys);
        setId(builder.id);
        setName(builder.name);
        setCod(builder.cod);
    }

    /**
     * @return  The coord
     */
    public Coord getCoord() {
        return coord;
    }

    /**
     * @param  coord  The coord
     */
    public void setCoord(final Coord coord) {
        this.coord = coord;
    }

    /**
     * @return  The weather
     */
    public List<Weather> getWeather() {
        return weather;
    }

    /**
     * @param  weather  The weather
     */
    public void setWeather(final List<Weather> weather) {
        this.weather = weather;
    }

    /**
     * @return  The base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param  base  The base
     */
    public void setBase(final String base) {
        this.base = base;
    }

    /**
     * @return  The main
     */
    public Main getMain() {
        return main;
    }

    /**
     * @param  main  The main
     */
    public void setMain(final Main main) {
        this.main = main;
    }

    /**
     * @return  The wind
     */
    public Wind getWind() {
        return wind;
    }

    /**
     * @param  wind  The wind
     */
    public void setWind(final Wind wind) {
        this.wind = wind;
    }

    /**
     * @return  The clouds
     */
    public Clouds getClouds() {
        return clouds;
    }

    /**
     * @param  clouds  The clouds
     */
    public void setClouds(final Clouds clouds) {
        this.clouds = clouds;
    }

    /**
     * @return  The rain
     */
    public Rain getRain() {
        return rain;
    }

    /**
     * @param  rain  The rain
     */
    public void setRain(final Rain rain) {
        this.rain = rain;
    }

    /**
     * @return  The dt
     */
    public Integer getDt() {
        return dt;
    }

    /**
     * @param  dt  The dt
     */
    public void setDt(final Integer dt) {
        this.dt = dt;
    }

    /**
     * @return  The sys
     */
    public Sys getSys() {
        return sys;
    }

    /**
     * @param  sys  The sys
     */
    public void setSys(final Sys sys) {
        this.sys = sys;
    }

    /**
     * @return  The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param  id  The id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @return  The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param  name  The name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return  The cod
     */
    public Integer getCod() {
        return cod;
    }

    /**
     * @param  cod  The cod
     */
    public void setCod(final Integer cod) {
        this.cod = cod;
    }

    public static final class Builder {
        private Coord coord;
        private List<Weather> weather;
        private String base;
        private Main main;
        private Wind wind;
        private Clouds clouds;
        private Rain rain;
        private Integer dt;
        private Sys sys;
        private Integer id;
        private String name;
        private Integer cod;

        public Builder() { }

        public Builder coord(final Coord val) {
            coord = val;
            return this;
        }

        public Builder weather(final List<Weather> val) {
            weather = val;
            return this;
        }

        public Builder base(final String val) {
            base = val;
            return this;
        }

        public Builder main(final Main val) {
            main = val;
            return this;
        }

        public Builder wind(final Wind val) {
            wind = val;
            return this;
        }

        public Builder clouds(final Clouds val) {
            clouds = val;
            return this;
        }

        public Builder rain(final Rain val) {
            rain = val;
            return this;
        }

        public Builder dt(final Integer val) {
            dt = val;
            return this;
        }

        public Builder sys(final Sys val) {
            sys = val;
            return this;
        }

        public Builder id(final Integer val) {
            id = val;
            return this;
        }

        public Builder name(final String val) {
            name = val;
            return this;
        }

        public Builder cod(final Integer val) {
            cod = val;
            return this;
        }

        public WeatherResponse build() {
            return new WeatherResponse(this);
        }
    }
}
