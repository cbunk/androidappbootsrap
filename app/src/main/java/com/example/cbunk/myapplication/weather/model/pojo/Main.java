
package com.example.cbunk.myapplication.weather.model.pojo;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Main {

    @SerializedName("temp")
    @Expose
    private Double temp;
    @SerializedName("pressure")
    @Expose
    private Double pressure;
    @SerializedName("humidity")
    @Expose
    private Integer humidity;
    @SerializedName("temp_min")
    @Expose
    private Double tempMin;
    @SerializedName("temp_max")
    @Expose
    private Double tempMax;

    private Main(final Builder builder) {
        setTemp(builder.temp);
        setPressure(builder.pressure);
        setHumidity(builder.humidity);
        setTempMin(builder.tempMin);
        setTempMax(builder.tempMax);
    }

    /**
     * @return  The temp in Kelvin
     */
    public Double getTemp() {
        return temp;
    }

    public Double getTemp(@Temp.Unit final int unit) {
        return Temp.convert(getTemp(), unit);
    }

    /**
     * @param  temp  The temp
     */
    public void setTemp(final Double temp) {
        this.temp = temp;
    }

    /**
     * @return  The pressure
     */
    public Double getPressure() {
        return pressure;
    }

    /**
     * @param  pressure  The pressure
     */
    public void setPressure(final Double pressure) {
        this.pressure = pressure;
    }

    /**
     * @return  The humidity
     */
    public Integer getHumidity() {
        return humidity;
    }

    /**
     * @param  humidity  The humidity
     */
    public void setHumidity(final Integer humidity) {
        this.humidity = humidity;
    }

    /**
     * @return  The tempMin
     */
    public Double getTempMin() {
        return tempMin;
    }

    public Double getTempMin(@Temp.Unit final int unit) {
        return Temp.convert(getTempMin(), unit);
    }

    /**
     * @param  tempMin  The temp_min
     */
    public void setTempMin(final Double tempMin) {
        this.tempMin = tempMin;
    }

    /**
     * @return  The tempMax
     */
    public Double getTempMax() {
        return tempMax;
    }

    public Double getTempMax(@Temp.Unit final int unit) {
        return Temp.convert(getTempMax(), unit);
    }

    /**
     * @param  tempMax  The temp_max
     */
    public void setTempMax(final Double tempMax) {
        this.tempMax = tempMax;
    }

    public static final class Builder {
        private Double temp;
        private Double pressure;
        private Integer humidity;
        private Double tempMin;
        private Double tempMax;

        public Builder() { }

        public Builder temp(final Double val) {
            temp = val;
            return this;
        }

        public Builder pressure(final Double val) {
            pressure = val;
            return this;
        }

        public Builder humidity(final Integer val) {
            humidity = val;
            return this;
        }

        public Builder tempMin(final Double val) {
            tempMin = val;
            return this;
        }

        public Builder tempMax(final Double val) {
            tempMax = val;
            return this;
        }

        public Main build() {
            return new Main(this);
        }
    }
}
