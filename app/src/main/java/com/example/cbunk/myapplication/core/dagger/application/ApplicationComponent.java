package com.example.cbunk.myapplication.core.dagger.application;

import javax.inject.Singleton;

import com.example.cbunk.myapplication.core.BaseApplication;
import com.example.cbunk.myapplication.core.otto.OttoEventBus;
import com.example.cbunk.myapplication.weather.model.WeatherService;

import dagger.Component;

/**
 * Created by cbunk on 15.12.15.
 */
@Singleton
@Component(modules = {ApplicationModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    OttoEventBus ottoEventBus();

    void inject(BaseApplication application);

    WeatherService weatherService();
}
