package com.example.cbunk.myapplication.core.dagger.application;

import javax.inject.Singleton;

import com.example.cbunk.myapplication.core.retrofit.RetrofitConfiguration;
import com.example.cbunk.myapplication.weather.model.WeatherService;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

import retrofit.Retrofit;

@Module
public class RetrofitModule {

    @Provides
    @Singleton
    public Retrofit provideRetrofit(final Context context) {
        return RetrofitConfiguration.retrofit(context);
    }

    @Provides
    @Singleton
    public WeatherService provideWeatherService(final Retrofit retrofit) {
        return retrofit.create(WeatherService.class);
    }
}
