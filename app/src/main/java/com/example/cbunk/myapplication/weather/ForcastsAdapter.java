package com.example.cbunk.myapplication.weather;

import java.util.List;

import com.example.cbunk.myapplication.core.BaseAdapter;
import com.example.cbunk.myapplication.weather.model.pojo.DailyForecast;

import android.support.v7.widget.RecyclerView;

import android.view.ViewGroup;

public class ForcastsAdapter extends BaseAdapter<DailyForecast> {
    public ForcastsAdapter(final List<DailyForecast> list) {
        super(list);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position,
            final DailyForecast forecast) {
        if (holder instanceof ForecastsViewHolder) {
            ForecastsViewHolder forecastsViewHolder = (ForecastsViewHolder) holder;
            forecastsViewHolder.bindData(forecast, position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return ForecastsViewHolder.create(parent);
    }
}
