package com.example.cbunk.myapplication.weather;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.BaseViewHolder;
import com.example.cbunk.myapplication.weather.model.pojo.DailyForecast;
import com.example.cbunk.myapplication.weather.model.pojo.Temp;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import butterknife.Bind;

public class ForecastsViewHolder extends BaseViewHolder<DailyForecast> {

    private final Context context;
    @Bind(R.id.forecast_item_day_textview)
    TextView dayTextView;

    @Bind(R.id.forecast_item_description_textview)
    TextView descriptionTextView;

    @Bind(R.id.forecast_item_max_temp_textview)
    TextView maxTempTextView;

    @Bind(R.id.forecast_item_min_temp_textview)
    TextView minTempTextView;

    public static ForecastsViewHolder create(final ViewGroup viewGroup) {
        return new ForecastsViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.forecasts_item_view,
                    viewGroup, false));
    }

    private ForecastsViewHolder(final View itemView) {
        super(itemView);

        context = itemView.getContext();
    }

    @Override
    protected void bindData(final DailyForecast forecast, final int position) {

        Long maxTemp = Math.round(forecast.getTemp().getMax(Temp.CELSIUS));
        maxTempTextView.setText(maxTemp.toString());

        Long minTemp = Math.round(forecast.getTemp().getMin(Temp.CELSIUS));
        minTempTextView.setText(minTemp.toString());

        descriptionTextView.setText(forecast.getWeather().get(0).getDescription());

        final DayFormatter dayFormatter = new DayFormatter(context);
        final String day = dayFormatter.format(forecast.getDt());

        dayTextView.setText(day);
    }
}
