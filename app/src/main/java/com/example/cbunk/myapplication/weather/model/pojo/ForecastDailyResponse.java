
package com.example.cbunk.myapplication.weather.model.pojo;

import java.util.ArrayList;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ForecastDailyResponse {

    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private Double message;
    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("cnt")
    @Expose
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<DailyForecast> dailyForecast = new ArrayList<DailyForecast>();

    /**
     * @return  The cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @param  cod  The cod
     */
    public void setCod(final String cod) {
        this.cod = cod;
    }

    /**
     * @return  The message
     */
    public Double getMessage() {
        return message;
    }

    /**
     * @param  message  The message
     */
    public void setMessage(final Double message) {
        this.message = message;
    }

    /**
     * @return  The city
     */
    public City getCity() {
        return city;
    }

    /**
     * @param  city  The city
     */
    public void setCity(final City city) {
        this.city = city;
    }

    /**
     * @return  The cnt
     */
    public Integer getCnt() {
        return cnt;
    }

    /**
     * @param  cnt  The cnt
     */
    public void setCnt(final Integer cnt) {
        this.cnt = cnt;
    }

    /**
     * @return  The list
     */
    public java.util.List<DailyForecast> getDailyForecast() {
        return dailyForecast;
    }

    /**
     * @param  dailyForecast  The list
     */
    public void setDailyForecast(final java.util.List<DailyForecast> dailyForecast) {
        this.dailyForecast = dailyForecast;
    }

}
