package com.example.cbunk.myapplication.weather;

import com.example.cbunk.myapplication.weather.model.pojo.ForecastDailyResponse;
import com.example.cbunk.myapplication.weather.model.pojo.WeatherResponse;

import rx.Observable;

public interface WeatherContract {

    interface View {
        void showCurrentWeather(final WeatherResponse weatherResponse);

        void showWeatherForecast(final ForecastDailyResponse forecastDailyResponse);
    }

    interface Presenter {
        void getCurrentWeather(final int cityId);

        void getWeatherForecast(final int cityId);
    }

    interface Model {

        interface LoadCurrentWeatherCallback {
            void onCurrentWeatherLoaded(final WeatherResponse weatherResponse);
        }

        Observable<WeatherResponse> getCurrentWeather(final int cityId);

        interface LoadWeatherForecastCallback {
            void onWeatherForecastLoaded(final ForecastDailyResponse forecastDailyResponse);
        }

        Observable<ForecastDailyResponse> getWeatherForecast(int cityId);
    }
}
