package com.example.cbunk.myapplication.weather.model;

import com.example.cbunk.myapplication.weather.model.pojo.ForecastDailyResponse;
import com.example.cbunk.myapplication.weather.model.pojo.WeatherResponse;

import retrofit.http.GET;
import retrofit.http.Query;

import rx.Observable;

public interface WeatherService {

    @GET("weather")
    Observable<WeatherResponse> currentWeatherObservable(@Query("id") int cityId);

    @GET("forecast/daily")
    Observable<ForecastDailyResponse> dailyForecastObservable(@Query("id") int cityId);
}
