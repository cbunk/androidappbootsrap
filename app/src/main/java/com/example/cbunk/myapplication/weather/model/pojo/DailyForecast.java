
package com.example.cbunk.myapplication.weather.model.pojo;

import java.util.ArrayList;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class DailyForecast {

    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("temp")
    @Expose
    private Temp temp;
    @SerializedName("pressure")
    @Expose
    private Double pressure;
    @SerializedName("humidity")
    @Expose
    private Integer humidity;
    @SerializedName("weather")
    @Expose
    private java.util.List<Weather> weather = new ArrayList<Weather>();

    /**
     * @return  The dt
     */
    public Integer getDt() {
        return dt;
    }

    /**
     * @param  dt  The dt
     */
    public void setDt(final Integer dt) {
        this.dt = dt;
    }

    /**
     * @return  The temp
     */
    public Temp getTemp() {
        return temp;
    }

    /**
     * @param  temp  The temp
     */
    public void setTemp(final Temp temp) {
        this.temp = temp;
    }

    /**
     * @return  The pressure
     */
    public Double getPressure() {
        return pressure;
    }

    /**
     * @param  pressure  The pressure
     */
    public void setPressure(final Double pressure) {
        this.pressure = pressure;
    }

    /**
     * @return  The humidity
     */
    public Integer getHumidity() {
        return humidity;
    }

    /**
     * @param  humidity  The humidity
     */
    public void setHumidity(final Integer humidity) {
        this.humidity = humidity;
    }

    /**
     * @return  The weather
     */
    public java.util.List<Weather> getWeather() {
        return weather;
    }

    /**
     * @param  weather  The weather
     */
    public void setWeather(final java.util.List<Weather> weather) {
        this.weather = weather;
    }

}
