package com.example.cbunk.myapplication.weather;

import java.text.MessageFormat;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;

import com.example.cbunk.myapplication.R;
import com.example.cbunk.myapplication.core.BaseActivity;
import com.example.cbunk.myapplication.core.LogUtils;
import com.example.cbunk.myapplication.core.dagger.activity.ActivityComponent;
import com.example.cbunk.myapplication.weather.model.WeatherModel;
import com.example.cbunk.myapplication.weather.model.pojo.DailyForecast;
import com.example.cbunk.myapplication.weather.model.pojo.ForecastDailyResponse;
import com.example.cbunk.myapplication.weather.model.pojo.Temp;
import com.example.cbunk.myapplication.weather.model.pojo.WeatherResponse;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

import android.os.Bundle;

import android.support.annotation.NonNull;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.widget.TextView;

import butterknife.Bind;

/**
 * Created by cbunk on 14.01.16.
 */
public class WeatherActivity extends BaseActivity implements WeatherContract.View {

    private static final Logger LOG = LogUtils.logger(WeatherActivity.class);
    private static final int BERLIN_CITY_ID = 2950159;

    private RangeMap<Long, Integer> celsiusColorRangeMap;

    @Bind(R.id.weather_activity_city_textview)
    TextView cityTextView;

    @Bind(R.id.weather_activity_description_textview)
    TextView descriptionTextView;

    @Bind(R.id.weather_activity_temperature_textview)
    TextView temperatureTextView;

    @Bind(R.id.weather_activity_max_temperature_textview)
    TextView temperatureMaxTextView;

    @Bind(R.id.weather_activity_min_temperature_textview)
    TextView temperatureMinTextView;

    @Bind(R.id.weather_activity_recycler_view)
    RecyclerView recyclerView;

    @Inject
    WeatherModel weatherModel;

    WeatherPresenter weatherPresenter;

    @Override
    protected int layoutToInflate() {
        return R.layout.weather_activity;
    }

    @Override
    protected void doInjection(final ActivityComponent component) {
        component.inject(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        weatherPresenter = new WeatherPresenter(this, weatherModel);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        celsiusColorRangeMap = createTemperatureColorScale();
    }

    @Override
    protected void onStart() {
        super.onStart();

        weatherPresenter.getCurrentWeather(BERLIN_CITY_ID);
        weatherPresenter.getWeatherForecast(BERLIN_CITY_ID);
    }

    @NonNull
    private RangeMap<Long, Integer> createTemperatureColorScale() {
        final RangeMap<Long, Integer> celsiusColorScale = TreeRangeMap.create();
        celsiusColorScale.put(Range.closed(-273L, -10L), getResources().getColor(R.color.temp_minus_10));
        celsiusColorScale.put(Range.closed(-9L, -5L), getResources().getColor(R.color.temp_minus_5));
        celsiusColorScale.put(Range.closed(-4L, 0L), getResources().getColor(R.color.temp_plus_0));
        celsiusColorScale.put(Range.closed(1L, 5L), getResources().getColor(R.color.temp_plus_5));
        celsiusColorScale.put(Range.closed(6L, 10L), getResources().getColor(R.color.temp_plus_10));
        celsiusColorScale.put(Range.closed(11L, 15L), getResources().getColor(R.color.temp_plus_15));
        celsiusColorScale.put(Range.closed(16L, 20L), getResources().getColor(R.color.temp_plus_20));
        celsiusColorScale.put(Range.closed(21L, 25L), getResources().getColor(R.color.temp_plus_25));
        celsiusColorScale.put(Range.closed(26L, 30L), getResources().getColor(R.color.temp_plus_30));
        celsiusColorScale.put(Range.closed(31L, 100L), getResources().getColor(R.color.temp_plus_35));
        return celsiusColorScale;
    }

    @NonNull
    private String addDegreeSymbol(final Long currentTemp) {
        return MessageFormat.format("{0}°", currentTemp);
    }

    @Override
    public void showCurrentWeather(final WeatherResponse weatherResponse) {
        cityTextView.setText(weatherResponse.getName());

        descriptionTextView.setText(weatherResponse.getWeather().get(0).getDescription());

        Long currentTemp = Math.round(weatherResponse.getMain().getTemp(Temp.CELSIUS));

        Integer color = celsiusColorRangeMap.get(currentTemp);
        if (color != null) {
            temperatureTextView.setTextColor(color);
        }

        temperatureTextView.setText(addDegreeSymbol(currentTemp));

        Long minTemp = Math.round(weatherResponse.getMain().getTempMin(Temp.CELSIUS));
        temperatureMinTextView.setText(addDegreeSymbol(minTemp));

        Long maxTemp = Math.round(weatherResponse.getMain().getTempMax(Temp.CELSIUS));
        temperatureMaxTextView.setText(addDegreeSymbol(maxTemp));
    }

    @Override
    public void showWeatherForecast(final ForecastDailyResponse forecastDailyResponse) {
        List<DailyForecast> forecasts = forecastDailyResponse.getDailyForecast();

        // 0 is today
        forecasts.remove(0);

        recyclerView.setAdapter(new ForcastsAdapter(forecasts));
    }

}
