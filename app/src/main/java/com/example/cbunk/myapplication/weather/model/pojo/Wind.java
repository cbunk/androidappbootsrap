
package com.example.cbunk.myapplication.weather.model.pojo;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Wind {

    @SerializedName("speed")
    @Expose
    private Double speed;
    @SerializedName("deg")
    @Expose
    private Double deg;

    /**
     * @return  The speed
     */
    public Double getSpeed() {
        return speed;
    }

    /**
     * @param  speed  The speed
     */
    public void setSpeed(final Double speed) {
        this.speed = speed;
    }

    /**
     * @return  The deg
     */
    public Double getDeg() {
        return deg;
    }

    /**
     * @param  deg  The deg
     */
    public void setDeg(final double deg) {
        this.deg = deg;
    }

}
