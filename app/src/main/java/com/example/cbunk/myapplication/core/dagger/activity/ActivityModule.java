package com.example.cbunk.myapplication.core.dagger.activity;

import com.example.cbunk.myapplication.core.BaseActivity;
import com.example.cbunk.myapplication.core.BaseApplication;

import android.app.ActionBar;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private final BaseActivity activity;

    public ActivityModule(final BaseActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityInstanceScope
    public BaseApplication provideApplication() {
        return (BaseApplication) activity.getApplicationContext();
    }

    @Provides
    @ActivityInstanceScope
    public ActionBar provideActionBar() {
        return activity.getActionBar();
    }

}
