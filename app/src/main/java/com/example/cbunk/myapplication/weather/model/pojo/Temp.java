
package com.example.cbunk.myapplication.weather.model.pojo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.support.annotation.IntDef;

@Generated("org.jsonschema2pojo")
public class Temp {

    @IntDef({ CELSIUS, FAHRENHEIT, KELVIN })
    @Retention(RetentionPolicy.SOURCE)
    public @interface Unit { }

    public static final int CELSIUS = 1;
    public static final int FAHRENHEIT = 2;
    public static final int KELVIN = 3;

    @SerializedName("day")
    @Expose
    private Double day;
    @SerializedName("min")
    @Expose
    private Double min;
    @SerializedName("max")
    @Expose
    private Double max;
    @SerializedName("night")
    @Expose
    private Double night;
    @SerializedName("eve")
    @Expose
    private Double eve;
    @SerializedName("morn")
    @Expose
    private Double morn;

    public static Double convert(final double temp, @Temp.Unit final int unit) {
        switch (unit) {

            case Temp.CELSIUS :
                return temp - 273.15;

            case Temp.FAHRENHEIT :
                return (temp - 273.15) * 1.8000 + 32;

            default :
                return temp;
        }
    }

    /**
     * @return  The day
     */
    public Double getDay() {
        return day;
    }

    public Double getDay(@Temp.Unit final int unit) {
        return convert(getDay(), unit);
    }

    /**
     * @param  day  The day
     */
    public void setDay(final Double day) {
        this.day = day;
    }

    /**
     * @return  The min
     */
    public Double getMin() {
        return min;
    }

    public Double getMin(@Temp.Unit final int unit) {
        return convert(getMin(), unit);
    }

    /**
     * @param  min  The min
     */
    public void setMin(final Double min) {
        this.min = min;
    }

    /**
     * @return  The max
     */
    public Double getMax() {
        return max;
    }

    public Double getMax(@Temp.Unit final int unit) {
        return convert(getMax(), unit);
    }

    /**
     * @param  max  The max
     */
    public void setMax(final Double max) {
        this.max = max;
    }

    /**
     * @return  The night
     */
    public Double getNight() {
        return night;
    }

    public Double getNight(@Temp.Unit final int unit) {
        return convert(getNight(), unit);
    }

    /**
     * @param  night  The night
     */
    public void setNight(final Double night) {
        this.night = night;
    }

    /**
     * @return  The eve
     */
    public Double getEve() {
        return eve;
    }

    public Double getEve(@Temp.Unit final int unit) {
        return convert(getEve(), unit);
    }

    /**
     * @param  eve  The eve
     */
    public void setEve(final Double eve) {
        this.eve = eve;
    }

    /**
     * @return  The morn
     */
    public Double getMorn() {
        return morn;
    }

    public Double getMorn(@Temp.Unit final int unit) {
        return convert(getMorn(), unit);
    }

    /**
     * @param  morn  The morn
     */
    public void setMorn(final Double morn) {
        this.morn = morn;
    }

}
