
package com.example.cbunk.myapplication.weather.model.pojo;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class City {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("coord")
    @Expose
    private Coord coord;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private Double message;
    @SerializedName("cnt")
    @Expose
    private Integer cnt;

    /**
     * @return  The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param  id  The id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @return  The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param  name  The name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return  The coord
     */
    public Coord getCoord() {
        return coord;
    }

    /**
     * @param  coord  The coord
     */
    public void setCoord(final Coord coord) {
        this.coord = coord;
    }

    /**
     * @return  The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param  country  The country
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @return  The cod
     */
    public String getCod() {
        return cod;
    }

    /**
     * @param  cod  The cod
     */
    public void setCod(final String cod) {
        this.cod = cod;
    }

    /**
     * @return  The message
     */
    public Double getMessage() {
        return message;
    }

    /**
     * @param  message  The message
     */
    public void setMessage(final Double message) {
        this.message = message;
    }

    /**
     * @return  The cnt
     */
    public Integer getCnt() {
        return cnt;
    }

    /**
     * @param  cnt  The cnt
     */
    public void setCnt(final Integer cnt) {
        this.cnt = cnt;
    }

}
