package com.example.cbunk.myapplication.core.retrofit;

import java.io.IOException;

import org.slf4j.Logger;

import com.example.cbunk.myapplication.core.LogUtils;

import com.google.common.base.Strings;

import com.squareup.okhttp.Headers;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

/**
 * Created by cbunk on 14.01.16.
 */
public class RewriteCacheControlInterceptor implements Interceptor {

    private static final Logger LOG = LogUtils.logger(RewriteCacheControlInterceptor.class);

    private static final String HEADER_ADD_CACHE_CONTROL_IF_ABSENT = "Add-Cache-Control-If-Absent";
    private static final String HEADER_FORCE_CACHE_CONTROL = "Force-Cache-Control";
    private static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final int DEFAULT_CACHING_TIME_IN_SECONDS = 3600;
    public static final String GET_HTTP_METHOD = "GET";

    @Override
    public Response intercept(final Chain chain) throws IOException {
        Request request = chain.request();

        // get the request headers for cache-control
        String addCacheControlIfAbsentHeader = request.header(HEADER_ADD_CACHE_CONTROL_IF_ABSENT);
        String forceCacheControlHeader = request.header(HEADER_FORCE_CACHE_CONTROL);

        Request.Builder requestBuilder = request.newBuilder();

        // remove the header to prevent sending them in request
        requestBuilder.removeHeader(HEADER_ADD_CACHE_CONTROL_IF_ABSENT);
        requestBuilder.removeHeader(HEADER_FORCE_CACHE_CONTROL);

        Response response = chain.proceed(requestBuilder.build());

        // stp when it is not successfull
        if (!response.isSuccessful()) {
            return response;
        }

        // after the request has been proceed we check the cache-control value from server
        String responseHeaderCacheControl = response.header(HEADER_CACHE_CONTROL);

        Headers.Builder headerBuilder = response.headers().newBuilder();

        // stop when its not get
        if (!request.method().equalsIgnoreCase(GET_HTTP_METHOD)) {
            return response;
        }

        // set default caching time
        String headerCacheControlValue = "";
        if (Strings.isNullOrEmpty(responseHeaderCacheControl)) {
            headerCacheControlValue = "public, max-age=" + DEFAULT_CACHING_TIME_IN_SECONDS;
        } else if (responseHeaderCacheControl.contains("max-age: ")) {
            headerCacheControlValue = responseHeaderCacheControl.replaceAll("max-age:", "max-age=")
                    + DEFAULT_CACHING_TIME_IN_SECONDS;
        }

        headerBuilder.set(HEADER_CACHE_CONTROL, headerCacheControlValue);

        // case if we need to force overwrite Cache-Control
        if (!Strings.isNullOrEmpty(forceCacheControlHeader)) {

            headerBuilder.set(HEADER_CACHE_CONTROL, "public, " + forceCacheControlHeader);

        } else if (Strings.isNullOrEmpty(responseHeaderCacheControl)
                && !Strings.isNullOrEmpty(addCacheControlIfAbsentHeader)) {

            // case if we need to add Cache-Control if server missed to send us
            headerBuilder.set(HEADER_CACHE_CONTROL, addCacheControlIfAbsentHeader);
        }

        return response.newBuilder().headers(headerBuilder.build()).build();
    }
}
