package com.example.cbunk.myapplication.core.dagger.activity;

import com.example.cbunk.myapplication.core.dagger.application.ApplicationComponent;
import com.example.cbunk.myapplication.weather.WeatherActivity;

import dagger.Component;

@ActivityInstanceScope
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface ActivityComponent {
    void inject(WeatherActivity weatherActivity);
}
