package com.example.cbunk.myapplication.core.retrofit;

import java.io.IOException;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

/**
 * Created by cbunk on 14.01.16.
 */
public class RequestInterceptor implements Interceptor {

    private static final String OPEN_WEATHER_API_KEY = "81fdc195a16528d12b521d30a9ddc199";

    @Override
    public Response intercept(final Chain chain) throws IOException {
        Request request = chain.request();

        HttpUrl httpUrl = request.httpUrl().newBuilder().addQueryParameter("APPID", OPEN_WEATHER_API_KEY).build();

        return chain.proceed(request.newBuilder().url(httpUrl).build());
    }
}
