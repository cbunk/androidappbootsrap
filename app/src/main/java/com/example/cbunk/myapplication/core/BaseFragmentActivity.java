package com.example.cbunk.myapplication.core;

import com.example.cbunk.myapplication.R;

import android.os.Bundle;

import android.support.v4.app.FragmentTransaction;

/**
 * Created by cbunk on 15.12.15.
 */
public abstract class BaseFragmentActivity extends BaseActivity {
    protected abstract BaseFragment contentFragment();

    @Override
    protected void onPostCreate(final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // if the device gets rotated we don't need to reload this fragment, cause the fragment manager will take care
        // of the last visible fragment
        if (savedInstanceState == null) {
            replaceContentFragment(contentFragment());
        }

    }

    protected int layoutToInflate() {
        return R.layout.base_activity;
    }

    public void replaceContentFragment(BaseFragment fragment) {

        if (fragment == null) {
            throw new IllegalArgumentException("fragment must not be null");
        }

        BaseFragment fragmentByTag = (BaseFragment) getSupportFragmentManager().findFragmentByTag(fragment.getClass()
                    .getSimpleName());

        if (fragmentByTag != null) {
            fragment = fragmentByTag;
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }
}
