package com.example.cbunk.myapplication.weather;

import org.slf4j.Logger;

import com.example.cbunk.myapplication.core.LogUtils;
import com.example.cbunk.myapplication.core.espresso.EspressoIdlingResource;
import com.example.cbunk.myapplication.core.rxjava.ObservableHelper;
import com.example.cbunk.myapplication.weather.model.pojo.ForecastDailyResponse;
import com.example.cbunk.myapplication.weather.model.pojo.WeatherResponse;

import com.google.common.base.Preconditions;

import rx.Subscriber;

public class WeatherPresenter implements WeatherContract.Presenter {

    private static final Logger LOG = LogUtils.logger(WeatherPresenter.class);

    private WeatherContract.Model weatherModel;
    private WeatherContract.View weatherView;

    public WeatherPresenter(final WeatherContract.View weatherView, final WeatherContract.Model weatherModel) {
        this.weatherModel = Preconditions.checkNotNull(weatherModel);
        this.weatherView = Preconditions.checkNotNull(weatherView);
    }

    @Override
    public void getCurrentWeather(final int cityId) {
        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.

        EspressoIdlingResource.increment(); // App is busy until further notice

        ObservableHelper.prepare(weatherModel.getCurrentWeather(cityId)).subscribe(new Subscriber<WeatherResponse>() {

                @Override
                public void onCompleted() {
                    EspressoIdlingResource.decrement();
                }

                @Override
                public void onError(final Throwable e) { }

                @Override
                public void onNext(final WeatherResponse weatherResponse) {
                    weatherView.showCurrentWeather(weatherResponse);
                }
            });

    }

    @Override
    public void getWeatherForecast(final int cityId) {
        EspressoIdlingResource.increment(); // App is busy until further notice

        ObservableHelper.prepare(weatherModel.getWeatherForecast(cityId)).subscribe(
            new Subscriber<ForecastDailyResponse>() {
                @Override
                public void onCompleted() {
                    EspressoIdlingResource.decrement(); // Set app as idle.
                }

                @Override
                public void onError(final Throwable e) { }

                @Override
                public void onNext(final ForecastDailyResponse forecastDailyResponse) {
                    weatherView.showWeatherForecast(forecastDailyResponse);
                }
            });
    }
}
