package com.example.cbunk.myapplication.weather.model;

import javax.inject.Inject;

import com.example.cbunk.myapplication.weather.WeatherContract;
import com.example.cbunk.myapplication.weather.model.pojo.ForecastDailyResponse;
import com.example.cbunk.myapplication.weather.model.pojo.WeatherResponse;

import rx.Observable;

public class WeatherModel implements WeatherContract.Model {

    private WeatherService weatherService;

    @Inject
    public WeatherModel(final WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @Override
    public Observable<WeatherResponse> getCurrentWeather(final int cityId) {
        return weatherService.currentWeatherObservable(cityId);
    }

    @Override
    public Observable<ForecastDailyResponse> getWeatherForecast(final int cityId) {
        return weatherService.dailyForecastObservable(cityId);
    }

}
