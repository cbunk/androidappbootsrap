package com.example.cbunk.myapplication.core.retrofit;

import java.io.File;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.content.Context;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class RetrofitConfiguration {

    static final String CLIENT_ID = "9622a5c14e987314cabe";
    static final String CLIENT_SECRET = "3edd72dda12afc7cbd528b2e741f74e51b9dc4e5";

    private static final String SHUTTERSTOCK_URL = "https://" + CLIENT_ID + ":" + CLIENT_SECRET
            + "@api.shutterstock.com/";
    private static final String API_VERSION = "v2/";

    private static final String CACHE_FILE_NAME = "responses";
    private static final int CACHE_SIZE = 10 * 1024 * 1024;

    public static Retrofit retrofit(final Context context) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl("http://api.openweathermap.org/data/2.5/");

        builder.addConverterFactory(GsonConverterFactory.create());
        builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());
        builder.client(createOkHttpClient(context));

        return builder.build();
    }

    private static OkHttpClient createOkHttpClient(final Context context) {
        OkHttpClient client = new OkHttpClient();

        client.interceptors().add(new RequestInterceptor());
        client.networkInterceptors().add(new RewriteCacheControlInterceptor());

        // setup cache
        File httpCacheDirectory = new File(context.getCacheDir(), CACHE_FILE_NAME);
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        // add cache to the client
        client.setCache(cache);
        return client;
    }
}
