
package com.example.cbunk.myapplication.weather.model.pojo;

import java.util.ArrayList;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class ForecastResponse {

    @SerializedName("city")
    @Expose
    private City city;

    @SerializedName("list")
    @Expose
    private java.util.List<Forecast> forecasts = new ArrayList<Forecast>();

    /**
     * @return  The city
     */
    public City getCity() {
        return city;
    }

    /**
     * @param  city  The city
     */
    public void setCity(final City city) {
        this.city = city;
    }

    /**
     * @return  The forecasts
     */
    public java.util.List<Forecast> getForecasts() {
        return forecasts;
    }

    /**
     * @param  forecasts  The forecasts
     */
    public void setForecasts(final java.util.List<Forecast> forecasts) {
        this.forecasts = forecasts;
    }
}
